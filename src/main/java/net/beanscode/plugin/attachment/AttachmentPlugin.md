### Attachment Plugin

Attachment plugin allows users to upload any file to a notebook as an entry (pdf, png, video etc.). It is 
a convenient way to add auxuliary files which might be relevant to a given notebook. 