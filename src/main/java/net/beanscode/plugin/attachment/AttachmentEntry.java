package net.beanscode.plugin.attachment;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.notebook.ReloadPropagator;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.file.LazyFileIterator;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.Meta;

public class AttachmentEntry extends NotebookEntry {

	public final String META_FILES = "META_FILES";
	public final String META_FILES_SNAPSHOT = "META_FILES_SNAPSHOT";
	
	public AttachmentEntry() {
		
	}

	@Override
	public String getShortDescription() {
		return "Attachment";
	}

	@Override
	public boolean isReloadNeeded() {
		return false;
	}

	@Override
	public ReloadPropagator reload() throws ValidationException, IOException {
		// do nothing
		return ReloadPropagator.NONBLOCK;
	}

	@Override
	public boolean isRunning() throws ValidationException, IOException {
		return false;
	}

	@Override
	public String getEditorClass() {
		return "net.beanscode.web.view.plugins.attachment.AttachmentPanel";
	}
	
	@Override
	public void remove() throws ValidationException, IOException {
		try {
			FileExt folder = new FileExt(BeansSettings.getPluginDefaultFolder(), "attachment/" + getId().getId());
			folder.delete();
		} catch (Exception e) {
			LibsLogger.error(AttachmentEntry.class, "Cannot remove folder: " + 
					new FileExt(BeansSettings.getPluginDefaultFolder(), "attachment/" + getId().getId()), e);
		}
		
		super.remove();
	}
	
	public NotebookEntry upload(FileExt file, String name) throws IOException {
		return upload(file.openInputStream(), notEmpty(name) ? name : file.getFilenameOnly());
	}
	
	@Override
	public AttachmentEntry upload(InputStream stream, String name) {
		try {
			// first save the file
			FileExt folder = getEntryFolder();
			folder.mkdirs();
			
			FileExt tmp = new FileExt(folder.getAbsolutePath(), name);
			LibsLogger.debug(AttachmentEntry.class, "Saving uploading file to ", tmp.getAbsolutePath());
			
			FileUtils.saveToFile(stream, tmp);
			LibsLogger.debug(AttachmentEntry.class, "Upload to ", tmp.getAbsolutePath(), " finished");
			
//			addFileTmp(tmp);
			updateFileListFromDisk();
			save();
			
//			NotebookEntryFactory.updateCache(this);
		} catch (Exception e) {
			LibsLogger.error(AttachmentEntry.class, "Cannot save file", e);
		}
		
		return this;
	}
	
	
	private FileExt getEntryFolder() {
		return new FileExt(BeansSettings.getPluginDefaultFolder(), "attachment/" + getId().getId());
	}
	
	public List<String> getAttachmentFiles() {
		List<String> files = new ArrayList<>();
		Meta filesMeta = getMeta().get(META_FILES);
		if (filesMeta != null)
			files = JsonUtils.fromJson(filesMeta.getAsString(), List.class);
		return files;
	}
		
	@Override
	public String getSummary() {
		return null;
	}
	
	private List<String> getRemovedFiles() {
		List<String> removedFiles = new ArrayList<>();
		for (Meta meta : getMeta()) {
			if (meta.getName().startsWith("removed-")) {
				removedFiles.add(meta.getAsString());
			}
		}
		return removedFiles;
	}
	
	private boolean removeFilesIfNeeded() throws IOException {
		boolean removed = false; 
		
		// removing all files and meta parameters: removed-...
		for (String f : getRemovedFiles()) {
			FileExt fileExt = new FileExt(getEntryFolder(), f);
			fileExt.deleteIfExists();
			removed = true;
		}
		
		getMeta().removeWithPrefix("removed-");
		
		return removed;
	}

	@Override
	public void stop() throws ValidationException, IOException {
		removeFilesIfNeeded();
		
		updateFileListFromDisk();
		
		save();
	}
	
	private void updateFileListFromDisk() {
		List<String> newFiles = new ArrayList<>();
		for (FileExt file : new LazyFileIterator(getEntryFolder(), false, true, false))
			newFiles.add(file.getFilenameOnly());
		setMeta(META_FILES, JsonUtils.objectToString(newFiles));
	}

	@Override
	public void start() throws ValidationException, IOException {
		removeFilesIfNeeded();
		
		updateFileListFromDisk();
		
		getMeta().copy(META_FILES, META_FILES_SNAPSHOT);
		
		getProgress()
			.ok()
			.save();
		
		save();
	}
}
