package net.beanscode.plugin.attachment;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.StorageManager;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Attachment {

	@NotNull
	@NotEmpty
	@Expose
	private String text = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private UUID attachmentId = null;
	
	public Attachment() {
		
	}
	
	public void upload(InputStream stream, String filename) throws IOException {
//		assertTrue(getFileId() != null, "FileID cannot be null or empty");
//		
//		StorageManager.save(getFileId(), stream);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public UUID getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(UUID attachmentId) {
		this.attachmentId = attachmentId;
	}

}
