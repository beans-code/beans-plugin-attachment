package net.beanscode.plugin.attachment;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.Func;
import net.beanscode.model.plugins.Plugin;
import net.beanscode.model.utils.MarkdownUtils;
import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchEngineProvider;
import net.hypki.libs5.utils.reflection.SystemUtils;

public class AttachmentPlugin implements Plugin {

	public AttachmentPlugin() {
		
	}

	@Override
	public String getName() {
		return "Attachments plugin";
	}

	@Override
	public String getVersion() {
		return "1.0.0";
	}

	@Override
	public String getDescription() {
		return MarkdownUtils.toHtml(SystemUtils.readFileContent("AttachmentPlugin.md"));
	}

	@Override
	public String getPanel(UUID userId) throws IOException {
		return MarkdownUtils.toHtml(SystemUtils.readFileContent("AttachmentPlugin.md"));
	}

	@Override
	public List<Func> getFuncList() {
		return null;
	}

	@Override
	public List<Class<? extends Connector>> getConnectorClasses() {
		return null;
	}

	@Override
	public List<Class<? extends NotebookEntry>> getNotebookEntries() {
		List<Class<? extends NotebookEntry>> entries = new ArrayList<Class<? extends NotebookEntry>>();
		entries.add(AttachmentEntry.class);
		return entries;
	}

	@Override
	public List<Class<? extends DatabaseProvider>> getDatabaseProviderList() {
		return null;
	}

	@Override
	public List<Class<? extends SearchEngineProvider>> getSearchEngineProviderList() {
		return null;
	}
	
	@Override
	public boolean selfTest(UUID userId, OutputStreamWriter output) throws ValidationException, IOException {
		output.write("TODO");
		return true;
	}
}
