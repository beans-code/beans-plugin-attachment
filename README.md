# Attachment plugin for BEANS

`BEANS` is a web-based software for interactive distributed data analysis with a clear interface for querying, filtering, aggregating, and plotting data from an arbitrary number of datasets and tables [https://gitlab.com/beans-code/beans](https://gitlab.com/beans-code/beans).

`Attachment plugin` allows to upload files to `BEANS` to notebooks. In this way one can upload additional files, like pdf, png, text files which might be relevant to any given notebook.
